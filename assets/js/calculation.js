$('.calc-spinner .calc-spinner-plus').click(function(){
	var calcSpinner = $(this).parent();
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');

	if ($.isNumeric(count)){
		count = parseInt(count) + 1;
		$(calcSpinner).find('.calc-spinner-count').val(count);
		$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
	}
});


$('.calc-spinner .calc-spinner-count').change(function(){
	var calcSpinner = $(this).parent().parent();
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');
	
	if (count% 1 ===0) {
		$(this).removeClass('arrow')
		if ($.isNumeric(count) && count > 0){
			$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
			$(this).removeClass('error');
		} else {
			$(this).addClass('error');
		}
	} else {
		$(this).addClass("error");
	}

});

$('.calc-spinner .calc-spinner-size').change(function(){
	var calcSpinner = $(this).parent().parent();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');
	
	if ($.isNumeric(dataSize) && size > 0){
		$(calcSpinner).find('.calc-spinner-count').val(Math.ceil(size/dataSize));
		$(this).removeClass('error');
	} else {
		$(this).addClass('error');
	}
});


$('.calc-spinner .calc-spinner-minus').click(function(){
	var calcSpinner = $(this).parent();
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');

	if ($.isNumeric(count)){
		if (parseInt(count) -1> 0) {
			count = parseInt(count) - 1;
			$(calcSpinner).find('.calc-spinner-count').val(count);
			$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
		}
	}
});
