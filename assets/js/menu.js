$('.header-nav-menu .main-link.catalog-link').hover(function(){
	$(this).addClass('active');
	$('.main-menu').addClass('main-menu-show');

},function(){
	$('.main-menu').removeClass('main-menu-show');
	$(this).removeClass('active');
	/*$('.main-menu').mouseleave(function(){
		$('.header-nav-menu .main-link.catalog-link').removeClass('active');
	});*/
});

$('.main-menu').hover(function(){
	$('.header-nav-menu .main-link.catalog-link').addClass('active');
},function(){
	$('.header-nav-menu .main-link.catalog-link').removeClass('active');
});


(function($) {

	$.fn.menuAim = function(opts) {

		this.each(function() {
			init.call(this, opts);
		});

		return this;
	};

	function init(opts) {
		var $menu = $(this),
		activeRow = null,
		mouseLocs = [],
		lastDelayLoc = null,
		timeoutId = null,
		options = $.extend({
			rowSelector: "> li",
			submenuSelector: "*",
			submenuDirection: "right",
			tolerance: 75,  
			enter: $.noop,
			exit: $.noop,
			activate: $.noop,
			deactivate: $.noop,
			exitMenu: $.noop
		}, opts);

		var MOUSE_LOCS_TRACKED = 3,  
		DELAY = 300;  


		var mousemoveDocument = function(e) {
			mouseLocs.push({x: e.pageX, y: e.pageY});

			if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
				mouseLocs.shift();
			}
		};


		var mouseleaveMenu = function() {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}


			if (options.exitMenu(this)) {
				if (activeRow) {
					options.deactivate(activeRow);
				}

				activeRow = null;
			}
		};


		var mouseenterRow = function() {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}

			options.enter(this);
			possiblyActivate(this);
		},
		mouseleaveRow = function() {
			options.exit(this);
		};


		var clickRow = function() {
			activate(this);
		};

		var activate = function(row) {
			if (row == activeRow) {
				return;
			}

			if (activeRow) {
				options.deactivate(activeRow);
			}

			options.activate(row);
			activeRow = row;
		};

		var possiblyActivate = function(row) {
			var delay = activationDelay();

			if (delay) {
				timeoutId = setTimeout(function() {
					possiblyActivate(row);
				}, delay);
			} else {
				activate(row);
			}
		};


		var activationDelay = function() {
			if (!activeRow || !$(activeRow).is(options.submenuSelector)) {

				return 0;
			}

			var offset = $menu.offset(),
			upperLeft = {
				x: offset.left,
				y: offset.top - options.tolerance
			},
			upperRight = {
				x: offset.left + $menu.outerWidth(),
				y: upperLeft.y
			},
			lowerLeft = {
				x: offset.left,
				y: offset.top + $menu.outerHeight() + options.tolerance
			},
			lowerRight = {
				x: offset.left + $menu.outerWidth(),
				y: lowerLeft.y
			},
			loc = mouseLocs[mouseLocs.length - 1],
			prevLoc = mouseLocs[0];

			if (!loc) {
				return 0;
			}

			if (!prevLoc) {
				prevLoc = loc;
			}

			if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
				prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {

				return 0;
		}

		if (lastDelayLoc &&
			loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {

			return 0;
	}

	function slope(a, b) {
		return (b.y - a.y) / (b.x - a.x);
	};

	var decreasingCorner = upperRight,
	increasingCorner = lowerRight;

	if (options.submenuDirection == "left") {
		decreasingCorner = lowerLeft;
		increasingCorner = upperLeft;
	} else if (options.submenuDirection == "below") {
		decreasingCorner = lowerRight;
		increasingCorner = lowerLeft;
	} else if (options.submenuDirection == "above") {
		decreasingCorner = upperLeft;
		increasingCorner = upperRight;
	}

	var decreasingSlope = slope(loc, decreasingCorner),
	increasingSlope = slope(loc, increasingCorner),
	prevDecreasingSlope = slope(prevLoc, decreasingCorner),
	prevIncreasingSlope = slope(prevLoc, increasingCorner);

	if (decreasingSlope < prevDecreasingSlope &&
		increasingSlope > prevIncreasingSlope) {

		lastDelayLoc = loc;
	return DELAY;
}

lastDelayLoc = null;
return 0;
};

$menu
.mouseleave(mouseleaveMenu)
.find(options.rowSelector)
.mouseenter(mouseenterRow)
.mouseleave(mouseleaveRow)
.click(clickRow);

$(document).mousemove(mousemoveDocument);

};
})(jQuery);



jQuery(document).ready(function($){

	$('.cd-dropdown-trigger').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});


	$('.cd-dropdown .cd-close').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});


	$('.has-children').children('span').on('click', function(event){
		var selected = $(this);
		selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
	});



	var submenuDirection = ( !$('.cd-dropdown-wrapper').hasClass('open-to-left') ) ? 'right' : 'left';
	$('.cd-dropdown-content').menuAim({
		activate: function(row) {
			$(row).children().addClass('is-active').removeClass('fade-out');
			if( $('.cd-dropdown-content .fade-in').length == 0 ) $(row).children('ul').addClass('fade-in');
		},
		deactivate: function(row) {
			$(row).children().removeClass('is-active');
			if( $('li.has-children').length == 0 || $('li.has-children').is($(row)) ) {
				$('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
				$(row).children('ul').addClass('fade-out')
			}
		},
		exitMenu: function() {
			$('.cd-dropdown-content').find('.is-active').removeClass('is-active');
			return true;
		},
		submenuDirection: submenuDirection,
	});


	$('.go-back').on('click', function(){
		var selected = $(this),
		visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
		selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
	}); 


	function toggleNav(){
		var navIsVisible = ( !$('.cd-dropdown').hasClass('dropdown-is-active') ) ? true : false;
		$('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
		$('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
		if( !navIsVisible ) {
			$('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
				$('.has-children ul').addClass('is-hidden');
				$('.move-out').removeClass('move-out');
				$('.is-active').removeClass('is-active');
			});	
		}
	}

});



/*scroll page*/

$(window).scroll(function() {
	if ($(document).scrollTop() > 144) {
		$(".header-nav").addClass('header-nav-fixed');
		$('main').css({
			"padding-top": "72px",
		});
	} else {
		$(".header-nav").removeClass('header-nav-fixed');
		$('main').removeAttr("style");
	}
});


/*show search*/
$('.header-min .header-min-search, .cd-dropdown-wrapper .header-min-search').click(function(){
	$('.header-min-form').show();
})

$('.header-min .header-min-form .btn-close').click(function(){
	$('.header-min-form').hide();
})