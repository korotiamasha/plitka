
$('.basket-calc-spinner .count-plus, .basket-calc-spinner .size-plus').click(function(){
	var calcSpinner = $(this).closest('div.basket-calc-spinner');
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');

	if ($.isNumeric(count)){
		$('.calc-spinner-count').removeClass('error');
		count = parseInt(count) + 1;
		$(calcSpinner).find('.calc-spinner-count').val(count);
		$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
	}
});

$('.basket-calc-spinner .calc-spinner-count').change(function(){
	var calcSpinner = $(this).closest('div.basket-calc-spinner');
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');
	
	if (count% 1 ===0) {
		$(this).removeClass('arrow')
		if ($.isNumeric(count) && count > 0){
			$(this).removeClass('error');
			$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
			
		} else {
			$(this).addClass('error');
		}
	} else {
		$(this).addClass("error");
	}
});

$('.basket-calc-spinner .calc-spinner-size').change(function(){
	var calcSpinner = $(this).closest('div.basket-calc-spinner');
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');
	
	if ($.isNumeric(dataSize) && size > 0){
		$(calcSpinner).find('.calc-spinner-count').val(Math.ceil(size/dataSize));
		$(this).removeClass('error');
	} else {
		$(this).addClass('error');
	}
});


$('.basket-calc-spinner .count-minus, .basket-calc-spinner .size-minus').click(function(){
	var calcSpinner = $(this).closest('div.basket-calc-spinner');
	var count = $(calcSpinner).find('.calc-spinner-count').val();
	var size = $(calcSpinner).find('.calc-spinner-size').val();
	var dataSize = $(calcSpinner).find('.calc-spinner-size').attr('data-size');

	if ($.isNumeric(count)){
		$('.calc-spinner-size').removeClass('error');
		if (parseInt(count) -1> 0) {
			count = parseInt(count) - 1;
			$(calcSpinner).find('.calc-spinner-count').val(count);
			$(calcSpinner).find('.calc-spinner-size').val((count*dataSize).toFixed(2));
		}
	}
});