$('.product .btn-call').click(function(){
	$(this).closest("div.product").find(".product-watch").show();
})

$(".product-watch .watch-close").click(function(){
	$(this).closest('div.product-watch').hide();
})

$('.product.product-disabled .product-availability').click(function(){
	$(this).closest("div.product-disabled").find(".product-call").show();
})

$(".product-call .call-close").click(function(){
	$(this).closest('div.product-call').hide();
})

$('.add-like').click(function(){
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
	} else {
		$(this).addClass('active');
	}
});

$('.datepicker').datepicker({
	clearBtn: true,
	language: "ru",
	minDate: new Date(),
	maxDate: "+1Y",
	startDate: new Date(),
	orientation: "bottom left",
	todayHighlight: true,
	autoclose: true,
});

$('.add-like').click(function(e){
	e.preventDefault();
});

$('#stepExample1').timepicker({ 'step': 60 });
$('#stepExample2').timepicker({
	'step': function(i) {
		return (i%2) ? 15 : 45;
	}
});

$(function(){
	$("#inputPhone").mask("+7 (999) 999 - 99 - 99",{placeholder:"+7 (___) ___ - __ - __ "});
	$("#inputNumber").mask("+7 (999) 999 - 99 - 99",{placeholder:"+7 (___) ___ - __ - __ "});
});


(function() {
	'use strict';
	window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
})();
