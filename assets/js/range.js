/*lenght-slider*/
var lenghtSlider = $("#lenght-slider").slider({
  value: [0, 100000],
}).on('slide', function(){

  var val = $(this).val().split(',');
  var min = val[0];
  var max = val[1];

  $('#input-lenght-min').val(min);
  $('#input-lenght-max').val(max);

});


$('#input-lenght-min, #input-lenght-max').keyup(function(){

  var min = $("#input-lenght-min").val();
  min = parseInt(min);
  var max = $("#input-lenght-max").val();
  max = parseInt(max);

  var sliderMin =$("#lenght-slider").data('slider-min');
  var sliderMax =$("#lenght-slider").data('slider-max');

  if( min > max ) {
    var tmp = max;
    max = min;
    min = tmp;

    console.log(min);
    console.log(max);
    $('#input-lenght-min').val(min).trigger('change');
    $('#input-lenght-max').val(max).trigger('change');
  }

  if (min < sliderMax && max < sliderMax && min > sliderMin && max > sliderMin) {
    $("#lenght-slider").slider('setValue', [parseInt(min), parseInt(max)]);
  }
});


/*width-slider*/
var widthSlider = $("#width-slider").slider().on('slide', function(){

  var val = $(this).val().split(',');
  var min = val[0];
  var max = val[1];

  $('#input-width-min').val(min);
  $('#input-width-max').val(max);

});


$('#input-width-min, #input-width-max').keyup(function(){

  var min = $("#input-width-min").val();
  min = parseInt(min);
  var max = $("#input-width-max").val();
  max = parseInt(max);

  var sliderMin =$("#width-slider").data('width-min');
  var sliderMax =$("#width-slider").data('width-max');

  if( min > max ) {
    var tmp = max;
    max = min;
    min = tmp;

    console.log(min);
    console.log(max);
    $('#input-width-min').val(min).trigger('change');
    $('#input-width-max').val(max).trigger('change');
  }

  if (min < sliderMax && max < sliderMax && min > sliderMin && max > sliderMin) {
    $("#width-slider").slider('setValue', [parseInt(min), parseInt(max)]);
  }
});


/*thickness-slider*/
var thicknessSlider = $("#thickness-slider").slider().on('slide', function(){

  var val = $(this).val().split(',');
  var min = val[0];
  var max = val[1];

  $('#input-thickness-min').val(min);
  $('#input-thickness-max').val(max);

});


$('#input-thickness-min, #input-thickness-max').keyup(function(){

  var min = $("#input-thickness-min").val();
  min = parseInt(min);
  var max = $("#input-thickness-max").val();
  max = parseInt(max);

  var sliderMin =$("#thickness-slider").data('thickness-min');
  var sliderMax =$("#thickness-slider").data('thickness-max');

  if( min > max ) {
    var tmp = max;
    max = min;
    min = tmp;

    console.log(min);
    console.log(max);
    $('#input-thickness-min').val(min).trigger('change');
    $('#input-thickness-max').val(max).trigger('change');
  }

  if (min < sliderMax && max < sliderMax && min > sliderMin && max > sliderMin) {
    $("#thickness-slider").slider('setValue', [parseInt(min), parseInt(max)]);
  }
});

/*price-slider*/
var priceSlider = $("#price-slider").slider().on('slide', function(){

  var val = $(this).val().split(',');
  var min = val[0];
  var max = val[1];

  $('#input-price-min').val(min);
  $('#input-price-max').val(max);

});


$('#input-price-min, #input-price-max').keyup(function(){

  var min = $("#input-price-min").val();
  min = parseInt(min);
  var max = $("#input-price-max").val();
  max = parseInt(max);

  var sliderMin =$("#price-slider").data('price-min');
  var sliderMax =$("#price-slider").data('price-max');

  if( min > max ) {
    var tmp = max;
    max = min;
    min = tmp;

    console.log(min);
    console.log(max);
    $('#input-price-min').val(min).trigger('change');
    $('#input-price-max').val(max).trigger('change');
  }

  if (min < sliderMax && max < sliderMax && min > sliderMin && max > sliderMin) {
    $("#price-slider").slider('setValue', [parseInt(min), parseInt(max)]);
  }
});


/*min-lenght-slider*/
var minLenghtSlider = $("#minLenght-slider").slider().on('slide', function(){

  var val = $(this).val().split(',');
  var min = val[0];
  var max = val[1];

  $('#input-minLenght-min').val(min);
  $('#input-minLenght-max').val(max);

});


$('#input-minLenght-min, #input-minLenght-max').keyup(function(){

  var min = $("#input-minLenght-min").val();
  min = parseInt(min);
  var max = $("#input-minLenght-max").val();
  max = parseInt(max);

  var sliderMin =$("#minLenght-slider").data('slider-min');
  var sliderMax =$("#minLenght-slider").data('slider-max');

  if( min > max ) {
    var tmp = max;
    max = min;
    min = tmp;

    console.log(min);
    console.log(max);
    $('#input-minLenght-min').val(min).trigger('change');
    $('#input-minLenght-max').val(max).trigger('change');
  }

  if (min < sliderMax && max < sliderMax && min > sliderMin && max > sliderMin) {
    $("#minLenght-slider").slider('setValue', [parseInt(min), parseInt(max)]);
  }
});


$(".check-box .show-more").click(function(){

  if($(this).attr('data-open')) {
    $(this).removeAttr('data-open');
    $(this).parent().find('.form-check-hide').show();
    $(this).parent().find('.form-check-hide').addClass('d-flex');
    $(this).html('Скрыть');
  } else {
    $(this).attr('data-open', 'true');
    $(this).parent().find('.form-check-hide').hide();
    $(this).parent().find('.form-check-hide').removeClass('d-flex');
    $(this).html('Показать все');
  }
});

/*sort active link*/
$(".catalog-table-sort .sort-btn, .filter-min-sort .sort-btn").click(function(){
  $(this).parent().find('.sort-btn.active').removeClass('active');
  $(this).addClass('active');
});


/*show filter*/
$('.btn-show-filter').click(function(){
  $('#filter-min').show();
  $("body").addClass("modal-open");
});

$('#filter-min .btn-header-close').click(function(){
  $('#filter-min').hide();
  $("body").removeClass("modal-open");
});

$('.filter-min-drop .filter-min-drop-btn').click(function(){
  if ($(this).hasClass('active')) {
    $(this).parent().find('.filter-min-drop-body').hide();
    $(this).removeClass('active');
  } else {
    $(this).parent().find('.filter-min-drop-body').show();
    $(this).addClass('active');
  }
});


/*popover*/
$('.category-table-filter .check-box .form-check-input').change(function(){
  if($('.category-table-filter .check-box .form-check-input:checked').length) {
    $('.filter-form-btn').show();
    var top = $(this).parent().position().top;

    var btnHeight = $('.filter-form-btn').height();
    $('.filter-form-btn').css({
      top: (parseInt(top) - btnHeight/10) +'px'
    });
  } else {
    $('.filter-form-btn').hide();
  }  
});


$('.filter-min-body .check-box .form-check-input').change(function(){
  if($('.filter-min-body .check-box .form-check-input:checked').length) {
    $('.filter-min-body .filter-form-btn').show();
    var top = $(this).parent().position().top;

    var btnHeight = $('.filter-form-btn').height();
    $('.filter-min-body .filter-form-btn').css({
      top: (parseInt(top) - btnHeight/10) +'px'
    });
  } else {
    $('.filter-min-body .filter-form-btn').hide();
  }  
});

