$('.product-card-gallery .min-images img').click(function(){
	var src = $(this).attr('data-src');
	$(this).parent().find('img.active').removeClass('active');
	$(this).addClass('active');

	$(this).closest("div.product-card-gallery").find('.big-image img').attr('src', src);
})