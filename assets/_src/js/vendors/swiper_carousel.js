$(function() {
  var swiperPartners = new Swiper('.partners-slider', {
    speed: 600,
    slidesPerView: 6,
    slidesPerGroup: 6,
    loop: true,
    pagination: {
      el: '.partners-pagination',
      clickable: true,
    },

    breakpoints: {
      320: {
        slidesPerView: 1,
        slidesPerGroup: 1,
      },

      576: {
        slidesPerView: 2,
        slidesPerGroup: 2,
      },

      769: {
        slidesPerView: 3,
        slidesPerGroup: 3,
      },

      1024: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },
    },
  });

  var swiperMain = new Swiper('.main-slider', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.main-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.main-next',
      prevEl: '.main-prev',
    },
  });

  var mySwiper = new Swiper('.slider-bath', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.bath-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.bath-next',
      prevEl: '.bath-prev',
    },
  });

  var swiperAdvantages = new Swiper('.advantages-slider', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.advantages-pagination',
      clickable: true,
    },
  });

  var swiperAdvantages = new Swiper('.swiper-interesting-product', {
    speed: 600,
    slidesPerView: 2,
    slidesPerGroup: 2,
    spaceBetween: 16,
    loop: true,
    pagination: {
      el: '.swiper-interesting-product-pagination',
      clickable: true,
    },

    breakpoints: {
      400: {
        slidesPerView: 1,
        slidesPerGroup: 1,
      }
    },
  });


  var swiperCollectionPage = new Swiper('.collection-page-slider', {
    speed: 600,
    slidesPerView: 6,
    slidesPerGroup: 6,
    spaceBetween: 12,
    loop: true,
    navigation: {
      nextEl: '.collection-page-next',
      prevEl: '.collection-page-prev',
    },

    breakpoints: {
      320: {
        slidesPerView: 1,
        slidesPerGroup: 1,
      },

      576: {
        slidesPerView: 2,
        slidesPerGroup: 2,
      },

      769: {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 8,
      },

      1024: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },
    },
  });

  /*gallery*/
  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 12,
    slidesPerView: 6,
    slidesPerGroup: 6,
    loop: true,
    freeMode: true,
    loopedSlides: 6,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      769: {
        slidesPerView: 4,
        spaceBetween: 8,
        loopedSlides: 4,
      },
    },
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    loop:true,
    loopedSlides: 5,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });

  /*address*/
  var swiperMain = new Swiper('.slider-address-1', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.slider-address-1-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.slider-address-1-next',
      prevEl: '.slider-address-1-prev',
    },
  });

  var swiperMain = new Swiper('.slider-address-2', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.slider-address-2-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.slider-address-2-next',
      prevEl: '.slider-address-2-prev',
    },
  });

  var swiperMain = new Swiper('.slider-address-3', {
    speed: 600,
    slidesPerView: 1,
    slidesPerGroup: 1,
    loop: true,
    pagination: {
      el: '.slider-address-3-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.slider-address-3-next',
      prevEl: '.slider-address-3-prev',
    },
  });



  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    var paneTarget = $(e.target).attr('href');
    var $thePane = $('.tab-pane' + paneTarget);
    var paneIndex = $thePane.index();
    if ($thePane.find('.swiper-container').length > 0 && 0 === $thePane.find('.swiper-slide-active').length) {
      mySwiper[paneIndex].update();
    }
  });

});
